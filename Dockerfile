FROM fedora:latest

RUN \
    dnf update -y --setopt=install_weak_deps=False; \
    dnf install -y \
        ninja-build \
        python3-devel \
        'python3dist(pip)' \
        \
        'python3dist(lxml)' \
        'python3dist(pillow)' \
        'python3dist(regex)' \
        'python3dist(ufolib2)' \
        \
        gcc-c++ \
        git \
        python27 \
	'python3dist(cu2qu)' \
        'python3dist(booleanoperations)' \
        'python3dist(compreffor)' \
        'python3dist(cython)' \
        'python3dist(virtualenv)' \
        \
        ttx \
        xmlstarlet \
    ; \
    dnf clean all;

RUN pip install 'nanoemoji >= 0.8.1'
