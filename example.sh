#!/bin/bash

set -o nounset
set -o errexit

version=12.4.0
tmp_dir=$(mktemp --tmpdir --directory nanoemoji-XXXXXX)

buildah build-using-dockerfile --tag=nanoemoji

(
    cd $tmp_dir
    if [[ ! -d openmoji ]]; then
        git clone --no-checkout https://github.com/hfg-gmuend/openmoji.git
    fi
    cd openmoji
    git config --local --type=bool advice.detachedHead false
    git checkout $version
)

# FIXME: Change glyf_colr_0 to glyf_colr_1 once
# https://github.com/googlefonts/colr-gradients-spec stabilises.
#
# FIXME: Use this to build SVG fonts instead of `scfbuild`.  Blocked by
# https://github.com/googlefonts/nanoemoji/issues/113
#
for format in glyf_colr_0; do
    mkdir -p $tmp_dir/openmoji/font/$format
    podman container run \
        --mount=type=bind,source=$tmp_dir/openmoji,destination=/mnt,relabel=private \
        --rm \
        --interactive \
        nanoemoji \
        sh -c "
            nanoemoji \
                --color_format=$format \
                --build_dir=/mnt/build \
                --output_file=/mnt/build/$format.ttf \
                /mnt/color/svg/*.svg
            xmlstarlet edit --inplace --update \
                '/ttFont/name/namerecord[@nameID=\"5\"][@platformID=\"3\"]' \
                --value '$version' \
                /mnt/font/OpenMoji-Color.ttx
            ttx \
                -m /mnt/build/$format.ttf \
                -o /mnt/font/$format/OpenMoji-Color.ttf \
                /mnt/font/OpenMoji-Color.ttx
        "
done
